import { ChakraProvider } from "@chakra-ui/react";
import Trello from "../src/components/HomePage/Trello";
import BoardsPage from "./components/EachBoard/BoardsPage";
import { Route, Routes } from "react-router-dom";
import Header from "./components/HomePage/Header";

function App() {
  return (
    <ChakraProvider>
      <Header />
      <Routes>
        <Route path="/" element={<Trello />} />

        <Route path="/board/:id" element={<BoardsPage />} />

        <Route path="*" element={<h1>Page Not Found</h1>} />
      </Routes>
    </ChakraProvider>
  );
}

export default App;
