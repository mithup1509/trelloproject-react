import React, { useEffect, useState } from "react";
import ModalForAdd from "./ModalForAdd";
import { Card, CardHeader, CardFooter, Heading, Flex } from "@chakra-ui/react";
import { DeleteIcon } from "@chakra-ui/icons";
import {
  createCardonListApi,
  deleteCardOnList,
  getAllCardsonList,
} from "../../service/BoardsAllDetails";
import CardsOnList from "./CardsOnList";
import { useToast } from "@chakra-ui/react";

function List(props) {
  const [cardName, setCardName] = useState("");
  const [cards, setCards] = useState([]);
  const toast = useToast();

  useEffect(() => {
    getAllCardsonList(props.id)
      .then((res) => {
        setCards(res);
      })
      .catch((err) => {
        toast({
          title: "Error.",
          description: `${err}`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  }, []);

  const handleCardName = (e) => {
    setCardName(e.target.value);
  };

  const addCard = () => {
    if (cardName === "") {
      return;
    }
    createCardonListApi(props.id, cardName)
      .then((res) => {
        setCards([...cards, res]);

        setCardName("");
      })
      .catch((err) => {
        toast({
          title: "Error.",
          description: `${err}`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  };

  const handleDeleteCards = (id) => {
    deleteCardOnList(id)
      .then(() => {
        setCards(
          cards.filter((element) => {
            return element.id !== id;
          })
        );
        toast({
          title: "Deleted Successfully.",
          description: `deleted card successfully`,
          status: "success",
          duration: 9000,
          isClosable: true,
        });
      })
      .catch((err) => {
        toast({
          title: "Error.",
          description: `${err}`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  };

  return (
    <Card
      align="center"
      minW={"20vw"}
      backgroundColor={"#ebecf0"}
      h={"100%"}
      m={2}
    >
      <CardHeader p={4} w={"100%"}>
        <Flex justifyContent={"space-between"}>
          <Heading fontSize={18}> {props.listTitle}</Heading>
          <DeleteIcon
            h={3.5}
            onClick={() => {
              props.onHandleDeleteList(props.id);
            }}
          />
        </Flex>
      </CardHeader>

      <Flex gap={2} w={"90%"} direction={"column"}>
        {" "}
        {cards.map((card) => {
          return (
            <CardsOnList
              card={card.name}
              key={card.id}
              onHandleDeleteCards={handleDeleteCards}
              id={card.id}
            />
          );
        })}
      </Flex>

      <CardFooter p={0} w={"100%"}>
        <ModalForAdd
          onHandleName={handleCardName}
          onHandleAdd={addCard}
          Name={cardName}
          type={"Card"}
        />
      </CardFooter>
    </Card>
  );
}

export default List;
