import React from "react";
import {
  Button,
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverHeader,
  PopoverBody,
  PopoverFooter,
  PopoverArrow,
  PopoverCloseButton,
  Input,
  useDisclosure,
} from "@chakra-ui/react";

function ModalForAdd(props) {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const handleClick = () => {
    props.onHandleAdd();
    onClose();
  };

  return (
    <>
      <Popover isOpen={isOpen} onClose={onClose}>
        <PopoverTrigger>
          <Button
            minW={"20vw"}
            mt={2}
            bg={"#DFE1E6"}
            cursor={"pointer"}
            _hover={{ bg: "white", color: "black" }}
            display={"flex"}
            color={"black"}
            justifyContent={"flex-start"}
            fontSize={15}
            h={10}
            onClick={onOpen}
          >
            {`+ Add ${props.type}`}
          </Button>
        </PopoverTrigger>

        <PopoverContent w={250}>
          <PopoverArrow />
          <PopoverHeader
            fontSize={12}
          >{`Create New ${props.type}`}</PopoverHeader>
          <PopoverCloseButton />
          <PopoverBody>
            <Input
              value={props.Name === "" ? "" : props.Name}
              placeholder={`${props.type} Name`}
              onChange={props.onHandleName}
              h={30}
              width={"90%"}
              p={0}
            />
          </PopoverBody>
          <PopoverFooter>
            <Button
              colorScheme="blue"
              onClick={handleClick}
              p={0}
              width={20}
              h={8}
            >
              Add
            </Button>
          </PopoverFooter>
        </PopoverContent>
      </Popover>
    </>
  );
}
export default ModalForAdd;
