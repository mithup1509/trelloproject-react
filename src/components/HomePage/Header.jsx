import React from "react";
import { Box, Flex, Text, Icon } from "@chakra-ui/react";
import { AiOutlineHome } from "react-icons/ai";
import { Link } from "react-router-dom";
import { Image } from "@chakra-ui/react";

function Header() {
  return (
    <Box
      className="headerbox"
      h={"3.5rem"}
      bg={'#2A2F4F'}
      p={2}
      w={'100vw'}
      boxShadow={"box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;"}
    >
      <Box>
        <Flex justify={"space-between"} align={"center"}>
          <Link to={"/"}>
            <Icon as={AiOutlineHome} color={'white'} w={"1.5rem"} h={"1.5rem"} mt={2} />
          </Link>
          <Box color={"#172B4D"}>
            <Link to={"/"}>
              <Image
                src="	https://a.trellocdn.com/prgb/assets/87e1af770a49ce8e84e3.gif"
                mt={5}
                h={5}
                alt="trello-image"
              />
            </Link>
          </Box>

          <Text fontSize={"1.5rem"} color={'white'}  mt={2}>
            mithup
          </Text>
        </Flex>
      </Box>
    </Box>
  );
}

export default Header;
