import { Box } from "@chakra-ui/react";
import React from "react";
import ModalComponentForAddBoard from "./ModalComponentForAddBoard";

function AddBoardButton(props) {
  return (
    <Box>
      <ModalComponentForAddBoard
        onHandleBoardNameInput={props.onHandleBoardNameInput}
        onCreateBoard={props.onCreateBoard}
      />
    </Box>
  );
}

export default AddBoardButton;
