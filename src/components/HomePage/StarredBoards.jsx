import React from "react";
import { Box, Flex } from "@chakra-ui/react";
import Board from "../EachBoard/Board";
import BoardType from "./BoardType";

function StarredBoards(props) {
  return (
    <Box w={"100%"} bg={"#ebecf0"}>
      <BoardType name={"Starred Board"} />
      <Box color={"black"}>
        <Flex wrap={"wrap"} justify={"flex-start"}>
          {props.boards
            .filter((element) => {
              return element.starred === true;
            })
            .map((board) => {
              return <Board board={board} key={board.id} />;
            })}
        </Flex>
      </Box>
    </Box>
  );
}

export default StarredBoards;
