import React, { useEffect, useState } from "react";

import {
  getAllBoards,
  createNewBoardApi,
} from "../../service/BoardsAllDetails";

import Boards from "./Boards";
import { Box, Flex, Text } from "@chakra-ui/react";
import BoardButton from "./BoardButton";
import { useToast } from "@chakra-ui/react";
import { Spinner } from "@chakra-ui/react";

const COLORS = [];

function Trello() {
  const [boards, setBoards] = useState([]);
  const [boardsValue, setBoardsValue] = useState(false);
  const toast = useToast();
  // TODO: No need to store in state. Create a glolabl variable in the component file/
  // TODO: COlors to be moved to the component which uses

  const [boardName, setBoardName] = useState("");

  useEffect(() => {
    getAllBoards()
      .then((res) => {
        setBoardsValue(true);
        setBoards(res);
      })
      .catch((err) => {
        toast({
          title: "Error",
          description: err,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
        return;
      });
  }, []);

  const handleBoardNameInput = (e) => {
    setBoardName(e.target.value);
  };

  const createBoard = (backgroudColor) => {
    if (backgroudColor === "" || boardName === "") {
      setBoardName("");
      return;
    }

    createNewBoardApi(boardName, backgroudColor)
      .then((data) => {
        setBoards([...boards, data]);
        setBoardName("");
      })
      .catch((err) => {
        toast({
          title: "Error",
          description: err,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
        return;
      });
  };

  return (
    <Box h={"100vh"} w={"100%"} bg={"#ebecf0"}>
      <Box w={"100%"} display={"flex"} justifyContent={"flex-end"}>
        <Box w={"70%"}>
          <Flex direction={"column"}>
            {/* Use a spinner component: TODO: */}
            {!boardsValue ? (
              <Flex align={"center"}>
                <Text>Data Loading...</Text>
                <Spinner color="blue.500" />
              </Flex>
            ) : (
              <>
                <Boards boards={boards} />

                <BoardButton
                  onHandleBoardNameInput={handleBoardNameInput}
                  onCreateBoard={createBoard}
                />
              </>
            )}
          </Flex>
        </Box>
      </Box>
    </Box>
  );
}

export default Trello;
