import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Box, Flex, Icon, Text } from "@chakra-ui/react";
import { StarIcon } from "@chakra-ui/icons";
// TODO: Change name of component
function Board(props) {
  const [value, setValue] = useState(false);

  const handleStarsDisplay = () => {
    setValue(true);
  };
  const handleStarsHide = () => {
    setValue(false);
  };

  return (
    <Box
      mx={"0.5%"}
      my={"2%"}
      onMouseEnter={handleStarsDisplay}
      onMouseLeave={handleStarsHide}
    >
      <Link to={`board/${props.board.id}`} state={props.board}>
        <Box
          borderRadius={5}
          w={{ base: "10rem", sm: "10rem", md: "12rem " }}
          h={"95px"}
          color={"white"}
          fontSize={16}
          fontWeight={700}
          backgroundPosition={"center"}
          backgroundRepeat={"no-repeat"}
          backgroundSize={"cover"}
          backgroundColor={`${props.board.prefs.background}`}
          backgroundImage={`url(${props.board.prefs.backgroundImage})`}
        >
          <Flex
            w={"100%"}
            h={"100%"}
            direction={"column"}
            justifyContent={"space-between"}
          >
            <Text m={1.5} fontSize={"1rem"}>
              {props.board.name}
            </Text>
            <Box>
              <Flex justify={"flex-end"}>
                <Icon
                  as={StarIcon}
                  color={value ? "yellow" : ""}
                  display={value ? "block" : "none"}
                />
              </Flex>
            </Box>
          </Flex>
        </Box>
      </Link>
    </Box>
  );
}

export default Board;
