import React, { useEffect, useState } from "react";
import {
  Button,
  Box,
  Modal,
  ModalCloseButton,
  ModalBody,
  ModalOverlay,
  ModalHeader,
  ModalContent,
  Input,
  useDisclosure,
} from "@chakra-ui/react";
import EachCheckList from "./EachCheckList";
import {
  getAllCheckListsonCard,
  createCheckListonCard,
  deleteCheckListonCard,
} from "../../service/BoardsAllDetails";
import { useToast } from "@chakra-ui/react";
import ModalForAdd from "./ModalForAdd";

function CheckListonCard(props) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const toast = useToast();

  const initialRef = React.useRef(null);
  const finalRef = React.useRef(null);

  const [checkListName, setCheckListName] = useState("");
  const [checkLists, setCheckLists] = useState([]);

  const handleCheckListName = (e) => {
    setCheckListName(e.target.value);
  };

  useEffect(() => {
    getAllCheckListsonCard(props.id)
      .then((res) => {
        setCheckLists(res);
      })
      .catch((err) => {
        toast({
          title: "Error.",
          description: `${err}`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  }, []);

  const addCheckList = () => {
    if (checkListName === "") {
      return;
    }
    createCheckListonCard(props.id, checkListName)
      .then((res) => {
        setCheckLists([...checkLists, res]);
        setCheckListName("");
      })
      .catch((err) => {
        toast({
          title: "Error.",
          description: `${err}`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  };

  const handleDeleteCheckList = (id) => {
    deleteCheckListonCard(props.id, id)
      .then(() => {
        setCheckLists(
          checkLists.filter((element) => {
            return element.id !== id;
          })
        );
      })
      .then(() => {
        toast({
          title: "Deleted Successfully.",
          description: `deleted checklist successfully`,
          status: "success",
          duration: 9000,
          isClosable: true,
        });
      })
      .catch((err) => {
        toast({
          title: "Error.",
          description: `${err}`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  };

  return (
    <>
      <Button
        onClick={onOpen}
        bg={"white"}
        w={"90%"}
        cursor={"pointer"}
        _hover={{ bg: "none" }}
      >
        <props.editablePreview />
        <Input
          border={"2px solid red"}
          p={0}
          m={0}
          as={props.editInput}
          onChange={props.onChangeName}
        />
      </Button>

      <Modal
        initialFocusRef={initialRef}
        finalFocusRef={finalRef}
        isOpen={isOpen}
        onClose={onClose}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{props.name}</ModalHeader>
          <ModalCloseButton />

          <ModalBody>
            {checkLists.map((checkList) => {
              return (
                <EachCheckList
                  onListName={checkList.name}
                  key={checkList.id}
                  id={checkList.id}
                  onHandleDeleteCheckList={handleDeleteCheckList}
                  cardId={props.cardId}
                />
              );
            })}
            <Box w={"100%"} textAlign={"right"}>
              <Button
                w={"30%"}
                bg={"grey"}
                h={10}
                m={2}
                onClick={() => {
                  props.onHandleDeleteCards(props.id);
                }}
              >
                Delete Card
              </Button>
            </Box>
          </ModalBody>
          <ModalForAdd
            onHandleAdd={addCheckList}
            onHandleName={handleCheckListName}
            type={"CheckList"}
            Name={checkListName}
          />
        </ModalContent>
      </Modal>
    </>
  );
}

export default CheckListonCard;
