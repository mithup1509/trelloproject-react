import React from "react";

import { Box, Card, Flex } from "@chakra-ui/react";

import EditCardName from "./EditCardName";

function CardsOnList(props) {
  return (
    <>
      <Card align="center">
        <Box
          bg={"white"}
          boxShadow={"box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;"}
          borderRadius={4}
          w={"100%"}
        >
          <Flex justifyContent={"space-between"} align={"center"}>
            <Box w={"100%"}>
              <EditCardName
                name={props.card}
                id={props.id}
                cardId={props.id}
                onHandleDeleteCards={props.onHandleDeleteCards}
              />
            </Box>
          </Flex>
        </Box>
      </Card>
    </>
  );
}

export default CardsOnList;
