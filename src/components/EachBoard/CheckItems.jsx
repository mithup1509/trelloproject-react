import { DeleteIcon } from "@chakra-ui/icons";
import { Box, Checkbox, Flex } from "@chakra-ui/react";

import React from "react";

function CheckItems(props) {
  return (
    <Box m={2} p={2}>
      <Box>
        <Flex justify={"space-between"} borderRadius={5} align={"center"}>
          <Checkbox
            defaultChecked={props.checkedState === "complete" ? true : false}
            onChange={(event) => {
              props.onHandleCheckListValue(event, props.id);
            }}
          >
            {props.items}
          </Checkbox>
          <DeleteIcon
            m={1}
            onClick={() => {
              props.onHandleDeleteChekListItems(props.id);
            }}
          />
        </Flex>
      </Box>
    </Box>
  );
}

export default CheckItems;
