import React from "react";
import { Box, Flex } from "@chakra-ui/react";

import Board from "../EachBoard/Board";
import BoardType from "./BoardType";

function YourBoards(props) {
  // TODO: Create a reusable component

  return (
    <Box w={"100%"} bg={"#ebecf0"}>
      <BoardType name={"Your Board"} />
      <Box color={"black"}>
        <Flex wrap={"wrap"} justify={"flex-start"}>
          {props.boards.map((board) => {
            return <Board board={board} id={board.id} key={board.id} />;
          })}
        </Flex>
      </Box>
    </Box>
  );
}

export default YourBoards;
