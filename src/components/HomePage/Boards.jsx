import React from "react";
import StarredBoards from "./StarredBoards";
import YourBoards from "./YourBoards";
import { Box } from "@chakra-ui/react";

function Boards(props) {
  return (
    <Box color="white" bgColor={"white"}>
      <StarredBoards boards={props.boards} />
      <YourBoards boards={props.boards} />
    </Box>
  );
}

export default Boards;
