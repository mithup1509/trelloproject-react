import React from "react";
import axios from "axios";

const key = "c1490959c1eced2587cfd40ba42b710e";
const token =
  "ATTA0a0915cfdf309c8b7e66ef1cf8266c382dddb963edb99a6ced466a82d6a3e3a002D874D7";

export function getAllBoards() {
  return axios
    .get(`https://api.trello.com/1/members/me/boards?key=${key}&token=${token}`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return err;
    });
}

export function createNewBoardApi(name, color) {
  return axios
    .post(
      `https://api.trello.com/1/boards/?name=${name}&prefs_background=${color}&key=${key}&token=${token}`
    )
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return err;
    });
}

export function getboardsListApi(id){
  return axios.get(`https://api.trello.com/1/boards/${id}/lists?key=${key}&token=${token}` ).then((res)=>{
    return res.data;
  }).catch((err)=>{
    return err;
  })
}


export function createlistApi(id,listname){
  return axios.post(`https://api.trello.com/1/lists?name=${listname}&idBoard=${id}&key=${key}&token=${token}`)
  .then((res)=>{
    return res.data;
  }).catch((err)=>{
    return err;
  })
}


export function deletelistApi(id){
  return axios.put(`https://api.trello.com/1/lists/${id}?closed=true&key=${key}&token=${token}`)
  .then((res)=>{
    return res.data;
  }).catch((err)=>{
    return err;
  })
}





export function getAllCardsonList(id){
  return axios.get(`https://api.trello.com/1/lists/${id}/cards?key=${key}&token=${token}`)
  .then((res)=>{
    return res.data;
  }).catch((err)=>{
    return err;
  })
}


export function createCardonListApi(id,cardname){
 return axios.post(`https://api.trello.com/1/cards?idList=${id}&name=${cardname}&key=${key}&token=${token}`).then((res)=>{
  return res.data;
 }).catch((err)=>{
  return err;
})
}


export function updateCardNameonListApi(cardid,newname){

return axios.put(`https://api.trello.com/1/cards/${cardid}?name=${newname}&key=${key}&token=${token}`).then((res)=>{
  return res;
}).catch((err)=>{
  return err;
})

}

export function deleteCardOnList(cardid){
  return axios.delete(`https://api.trello.com/1/cards/${cardid}?key=${key}&token=${token}`).then((res)=>{
    return res.data;
  }).catch((err)=>{
    return err;
  })
}



export function getAllCheckListsonCard(cardid){
  return axios.get(`https://api.trello.com/1/cards/${cardid}/checklists?key=${key}&token=${token}`).then((res)=>{
    return res.data;
  }).catch((err)=>{
    return err;
  })
}


export function createCheckListonCard(cardid,checklistname){
  return axios.post(`https://api.trello.com/1/cards/${cardid}/checklists?key=${key}&token=${token}&name=${checklistname}`).then((res)=>{
    return res.data;
  }).catch((err)=>{
    return err;
  })
}

export function deleteCheckListonCard(cardid,checklistid){
  return axios.delete(`https://api.trello.com/1/cards/${cardid}/checklists/${checklistid}?key=${key}&token=${token}`).then((res)=>{
    return res.data;
  }).catch((err)=>{
    return err;
  })
}



export function getAllCheckListItems(checklistid){
  return axios.get(`https://api.trello.com/1/checklists/${checklistid}?key=${key}&token=${token}`).then((res)=>{
    return res.data;
  }).then((res)=>{
    return res.checkItems;
  }).catch((err)=>{
    return err;
  })
}

export function createCheckItemonCheckList(checklistid,checkitemname){
  return axios.post(`https://api.trello.com/1/checklists/${checklistid}/checkItems?name=${checkitemname}&key=${key}&token=${token}`)
  .then((res)=>{
    return res.data;
  }).then((res)=>{
    return res;
  }).catch((err)=>{
    return err;
  })
}


export function deleteCheckListCheckItems(checklistid,checkitemid){
  return axios.delete(`https://api.trello.com/1/checklists/${checklistid}/checkItems/${checkitemid}?key=${key}&token=${token}`).then((res)=>{
    return res.data;
  }).catch((err)=>{
    return err;
  })
}



export function updateCheckListCheckItemState(cardid,checkItemId,stateOfCheckItem){
  return axios.put(`https://api.trello.com/1/cards/${cardid}/checkItem/${checkItemId}?state=${stateOfCheckItem}&key=${key}&token=${token}`).then((res)=>{
    return res.data;
  }).catch((err)=>{
    return err;
  })
}