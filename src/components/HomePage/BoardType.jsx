import React from "react";
import { Box, Flex, Icon, Text } from "@chakra-ui/react";
import { AiOutlineUser } from "react-icons/ai";

function BoardType(props) {
  return (
    <Box color={"black"}>
      <Flex align={"center"}>
        <Icon as={AiOutlineUser} w={5} h={3.5} />
        <Text className="typeofboard-text" fontWeight={700} fontSize={15}>
          {props.name}
        </Text>
      </Flex>
    </Box>
  );
}

export default BoardType;
