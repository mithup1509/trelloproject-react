import React from "react";
import { Box, Text } from "@chakra-ui/react";

function NameOfTheBoard(props) {
  return (
    <Box mt={0}  w={'100vw'}>
      <Text fontSize={20} fontWeight={700} textAlign={"center"} color={"white"}>
        {props.boardName}{" "}
      </Text>
    </Box>
  );
}

export default NameOfTheBoard;
