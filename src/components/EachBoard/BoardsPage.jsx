import { Box, Flex } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";

import List from "./List";
import { useParams } from "react-router-dom";
import { useToast } from "@chakra-ui/react";
import ModalForAdd from "./ModalForAdd";
import {
  getboardsListApi,
  createlistApi,
  deletelistApi,
} from "../../service/BoardsAllDetails";

import { useLocation } from "react-router-dom";
import NameOfTheBoard from "./NameOfTheBoard";

function BoardsPage() {
  const [listName, setListName] = useState("");
  const [list, setList] = useState([]);
  const toast = useToast();

  let boardId = useParams();
  // TODO:

  const location = useLocation();
  useEffect(() => {
    getboardsListApi(boardId.id)
      .then((res) => {
        setList(res);
      })
      .catch((err) => {
        // TODO: Display error
        toast({
          title: "Error.",
          description: `${err}`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  }, []);

  const handleListName = (e) => {
    setListName(e.target.value);
  };

  const addList = () => {
    if (listName === "") {
      return;
    }

    createlistApi(boardId.id, listName)
      .then((res) => {
        setList([...list, res]);
        setListName("");
      })
      .catch((err) => {
        toast({
          title: "Error.",
          description: `${err}`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
        // TODO:
      });
  };

  // TODO: handlehandleDeleteList

  const handleDeleteList = (id) => {
    deletelistApi(id)
      .then(() => {
        setList(
          list.filter((element) => {
            return element.id !== id;
          })
        );
      })
      .then(() => {
        toast({
          title: "Deleted Successfully.",
          description: `deleted list successfully`,
          status: "success",
          duration: 9000,
          isClosable: true,
        });
      })
      .catch((err) => {
        toast({
          title: "Error.",
          description: `${err}`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
    // TODO: Move inside the promise
  };

  return (
    <Box
      h={"100vh"}
      id={boardId}
      background={
        location.state.prefs.backgroundColor
          ? `${location.state.prefs.backgroundColor}`
          : `url(${location.state.prefs.backgroundImage})`
      }
      backgroundPosition={"center"}
      backgroundRepeat={"no-repeat"}
      backgroundSize={"cover"}
    >
      <NameOfTheBoard boardName={location.state.name} />

      <Box
        position={"absolute"}
        overflowX={"scroll"}
        mt={4}
        h={"100vh"}
        width={"100vw"}
      >
        <Flex justify={{ base: "center", md: "flex-start", sm: "flex-start" }}>
          {list.map((element) => {
            return (
              <List
                listTitle={element.name}
                onHandleDeleteList={handleDeleteList}
                key={element.id}
                id={element.id}
              />
            );
          })}

          <ModalForAdd
            onHandleName={handleListName}
            onHandleAdd={addList}
            Name={listName}
            type={"List"}
          />
        </Flex>
      </Box>
    </Box>
  );
}

export default BoardsPage;
