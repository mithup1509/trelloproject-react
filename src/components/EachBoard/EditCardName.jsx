import React, { useState } from "react";
import {
  Editable,
  EditableInput,
  EditablePreview,
  useEditableControls,
  ButtonGroup,
  IconButton,
  Box,
} from "@chakra-ui/react";
import { CheckIcon, CloseIcon, EditIcon } from "@chakra-ui/icons";
import CheckListonCard from "./CheckListonCard";
import { updateCardNameonListApi } from "../../service/BoardsAllDetails";

function EditCardName(props) {
  const [newName, setNewName] = useState("");
  const [previousName, setPreviousName] = useState(props.name);

  const changeName = (e) => {
    setNewName(e.target.value);
  };

  function handleClick() {
    updateCardNameonListApi(props.id, newName)
      .then(() => {
        setNewName(newName);
        setPreviousName(newName);
      })
      .catch((err) => {
        toast({
          title: "Error.",
          description: `${err}`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  }

  function handleCancelClick() {
    setNewName(previousName);
  }

  function EditableControls() {
    const {
      isEditing,
      getSubmitButtonProps,
      getCancelButtonProps,
      getEditButtonProps,
    } = useEditableControls();

    return isEditing ? (
      <ButtonGroup justifyContent="center" size="sm">
        <span onClick={handleClick}>
          <IconButton icon={<CheckIcon />} {...getSubmitButtonProps()} />
        </span>
        <span onClick={handleCancelClick}>
          <IconButton icon={<CloseIcon />} {...getCancelButtonProps()} />
        </span>
      </ButtonGroup>
    ) : (
      <IconButton
        size="sm"
        h={0}
        icon={<EditIcon />}
        {...getEditButtonProps()}
      />
    );
  }

  return (
    <Box>
      <Editable
        w={"100%"}
        value={newName ? newName : previousName}
        defaultValue={newName === "" ? previousName : newName}
        fontSize="15px"
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        isPreviewFocusable={false}
      >
        <CheckListonCard
          name={newName ? newName : props.name}
          id={props.id}
          cardId={props.cardId}
          onChangeName={changeName}
          editablePreview={EditablePreview}
          editInput={EditableInput}
          onHandleDeleteCards={props.onHandleDeleteCards}
        />

        <EditableControls />
      </Editable>
    </Box>
  );
}

export default EditCardName;
