import React, { useState } from "react";
import {
  Button,
  Modal,
  ModalCloseButton,
  ModalBody,
  ModalOverlay,
  ModalFooter,
  ModalHeader,
  ModalContent,
  FormControl,
  FormLabel,
  Input,
  useDisclosure,
  Box,
  Flex,
} from "@chakra-ui/react";
 const colors = [
    "blue",
    "orange",
    "green",
    "red",
    "purple",
    "pink",
    "lime",
    "grey",
  ];

function ModalComponentForAddBoard(props) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [backgroudColor, setBackgroudColor] = useState();

  const initialRef = React.useRef(null);
  const finalRef = React.useRef(null);

  function onCreate() {
    props.onCreateBoard(backgroudColor);
    onClose();
    setBackgroudColor("");
  }

  const handleClick = (e) => {
    setBackgroudColor(e.target.value);
  };

  return (
    <>
      <Button
        onClick={onOpen}
        border={"5px solid black"}
        w={{ base: "10rem", sm: "10rem", md: "12rem " }}
        h={"95px"}
        fontSize={16}
        fontWeight={700}
        bg={"white"}
        borderRadius={4}
        m={1}
      >
        + Add board
      </Button>

      <Modal
        initialFocusRef={initialRef}
        finalFocusRef={finalRef}
        isOpen={isOpen}
        onClose={onClose}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Create Your Board</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <FormControl>
              <FormLabel>Enter title of the Board</FormLabel>
              <Input
                ref={initialRef}
                placeholder="Board Name"
                onChange={props.onHandleBoardNameInput}
              />
            </FormControl>

            <Box m={0}>
              <ModalHeader mx={0} my={4} p={0}>
                Select Color
              </ModalHeader>
              <Flex wrap={"wrap"} gap={5}>
                {colors.map((color) => {
                  return (
                    <Button
                      border={
                        backgroudColor === color
                          ? "5px solid black"
                          : "1px solid black"
                      }
                      w={20}
                      h={"50px"}
                      bg={color}
                      key={color}
                      value={color}
                      onClick={handleClick}
                    ></Button>
                  );
                })}
              </Flex>
            </Box>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={onCreate}>
              Create
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default ModalComponentForAddBoard;
