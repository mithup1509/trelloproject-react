import { Box, Icon, Flex, Button } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import CheckItems from "./CheckItems";
import { Progress } from "@chakra-ui/react";
import { IoMdCheckboxOutline } from "react-icons/io";
import {
  getAllCheckListItems,
  createCheckItemonCheckList,
  deleteCheckListCheckItems,
  updateCheckListCheckItemState,
} from "../../service/BoardsAllDetails";
import { useToast } from "@chakra-ui/react";

import ModalForAdd from "./ModalForAdd";

function EachCheckList(props) {
  const [checkListItemName, setCheckListItemName] = useState("");
  const [checkListItems, setCheckListItems] = useState([]);
  const toast = useToast();

  useEffect(() => {
    getAllCheckListItems(props.id)
      .then((res) => {
        setCheckListItems(res);

      })
      .catch((err) => {
        toast({
          title: "Error.",
          description: `${err}`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  }, []);

  const handleCheckListItemName = (e) => {
    setCheckListItemName(e.target.value);
  };

  const addCheckListItems = () => {
    if (checkListItemName === "") {
      return;
    } else {
      createCheckItemonCheckList(props.id, checkListItemName)
        .then((res) => {
          setCheckListItems([...checkListItems, res]);
          setCheckListItemName("");
        })
        .catch((err) => {
          toast({
            title: "Error.",
            description: `${err}`,
            status: "error",
            duration: 9000,
            isClosable: true,
          });
        });
    }
  };

  const handleDeleteChekListItems = (id) => {
    deleteCheckListCheckItems(props.id, id)
      .then(() => {
        setCheckListItems([
          ...checkListItems.filter((item) => {
            return item.id !== id;
          }),
        ]);
    
      })
      .then(() => {
        toast({
          title: "Deleted Successfully.",
          description: `deleted checklist Item successfully`,
          status: "success",
          duration: 9000,
          isClosable: true,
        });
      })
      .catch((err) => {
        toast({
          title: "Error.",
          description: `${err}`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  };
  const checkListValue = (event, id) => {
    // TODO: Refactor

    updateCheckListCheckItemState(
      props.cardId,
      id,
      event.target.checked ? "complete" : "incomplete"
    )
      .then((res) => {
        const newChecklistItems = [...checkListItems];
        let indexval;
        newChecklistItems.forEach((element, index) => {
          if (element.id === id) {
            indexval = index;
          }
        });
        newChecklistItems[indexval] = res;
        setCheckListItems(newChecklistItems);
      })
      .catch((err) => {
        toast({
          title: "Error.",
          description: `${err}`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  };

  return (
    <Box m={1} p={0} w={"100%"} backgroundColor={"salmon"}>
      <Flex justify={"space-between"} borderRadius={5} align={"center"}>
        <Box display={"flex"} p={2} alignItems={"center"} gap={2}>
          <Icon as={IoMdCheckboxOutline} h={5} w={7} />
          <Box fontSize={20} fontWeight={700}>
            {props.onListName}
          </Box>
        </Box>
        <Button
          m={2}
          py={0}
          onClick={() => {
            props.onHandleDeleteCheckList(props.id);
          }}
        >
          Delete
        </Button>
      </Flex>
      <Box>
        <Progress
          value={
            (checkListItems.filter((element) => {
              return element.state === "complete" || element.state === true;
            }).length /
              checkListItems.length) *
            100
          }
          m={2}
        />
        {checkListItems.map((item) => {
          return (
            <CheckItems
              items={item.name}
              onHandleCheckListValue={checkListValue}
              checkedState={item.state}
              key={item.id}
              id={item.id}
              onHandleDeleteChekListItems={handleDeleteChekListItems}
            />
          );
        })}
      </Box>

      <ModalForAdd
        onHandleAdd={addCheckListItems}
        onHandleName={handleCheckListItemName}
        type={"CheckList Item"}
        Name={checkListItemName}
      />
    </Box>
  );
}

export default EachCheckList;
